/**
*功能，AdminLTE3 左侧菜单生成
*Author 283124296@qq.com 20210930
*/

;(function($){
    $.fn.AdminLTE3Menu = function(options){
        
        var defaults = {
            //各种参数，各种属性
			leafKey:"leaf", //是否是叶子节点 true/false
			childKey:"children", //子节点数组名key
			menuNameKey:"name", //菜单名称key
			menuURLKey:"url", //菜单链接key
			menuIdKey:"id", //菜单id key
			menuIconKey:"", //菜单icon key
			menuRightTextKey:"", //菜单右侧文本key
			deafultTrunkIcon:"nav-icon fas fa-circle",//默认树干菜单图标
			defaultLeafIcon:"far fa-circle nav-icon",//默认叶子图标
			menuData:[], //菜单数据 数组
        }

        var options = $.extend(defaults,options);
        //this.each(function(){	});
		creatMenu(options.menuData,options,$(this));
		
        return this;
    }

/**
* 创建菜单
*
*/
function creatMenu(menuData,options,container){
	console.log(menuData)
	for(let i = 0 ;i < menuData.length; i++){
		//当前节点的信息
		let isLeaf = menuData[i][options.leafKey];
        let hasChildtmp = menuData[i][options.childKey] ? true : false;
		let hasChild = (hasChildtmp && menuData[i][options.childKey].length>0) ? true : false;
		let menuText = menuData[i][options.menuNameKey];
		let menuId = menuData[i][options.menuIdKey];
		let menuIcon = options.menuIconKey ? menuData[i][options.menuIconKey] : false;
		let rightText = options.menuRightTextKey ? menuData[i][options.menuRightTextKey]?menuData[i][options.menuRightTextKey]:"" : "";
		
		//存在子节点，才为枝干节点
		if(hasChild){
			//是中间根节点
			let icon = menuIcon ? menuIcon : options.deafultTrunkIcon;
			let node  = '<li class="nav-item">'+
							'<a href="#" class="nav-link">'+
								'<i class="'+icon+'"></i>'+
								'<p>'+menuText+
									'<i class="right fas fa-angle-left"></i>'+
									'<span class="badge badge-info right">'+rightText+'</span>'+
								'</p>'+
							'</a>'+
							'<ul class="nav nav-treeview" id="menu'+menuId+'">'+
							'</ul>'+
						'</li>';
			$(container).append(node);
			creatMenu(menuData[i][options.childKey],options,$('#menu'+menuId));//递归构建子菜单
		}else{
			//是叶子节点
			let icon = menuIcon ? menuIcon : options.defaultLeafIcon;
			let path = menuData[i][options.menuURLKey];
			let leaf  = '<li class="nav-item">'+
							'<a href="'+path+'" class="nav-link" id="'+path.slice(1)+'">'+
								'<i class="'+icon+'"></i>'+
								'<p>'+menuText+
									'<span class="badge badge-info right">'+rightText+'</span>'+
								'</p>'+
							'</a>'+
						'</li>';
            $(container).append(leaf);
		}
	}
}


})(jQuery);

