import datetime

from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden, JsonResponse
from django.shortcuts import render
from user.models import User
import os
import csv
work_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
def login(req):
    """
    跳转登录
    :param req:
    :return:
    """
    return render(req, 'login.html')


def register(req):
    """
    跳转注册
    :param req:
    :return:
    """
    return render(req, 'register.html')


def index(req):
    """
    跳转首页
    :param req:
    :return:
    """
    username = req.session['username']
    total_user = len(User.objects.all())
    date = datetime.datetime.today()
    month = date.month
    year = date.year
    return render(req, 'index.html', locals())

def index2(req):
    """
    跳转首页
    :param req:
    :return:
    """
    username = req.session['username']
    total_user = len(User.objects.all())
    date = datetime.datetime.today()
    month = date.month
    year = date.year
    return render(req, 'welcome_index.html', locals())
def login_out(req):
    """
    注销登录
    :param req:
    :return:
    """
    del req.session['username']
    return HttpResponseRedirect('http://127.0.0.1:8000/')


def personal(req):
    username = req.session['username']
    role_id = req.session['role']
    user = User.objects.filter(name=username).first()
    return render(req, 'personal.html', locals())


def zydy(request):
    username = request.session['username']
    return render(request, 'zydyfx.html', locals())

def pffx(request):
    username = request.session['username']
    return render(request, 'pffx.html', locals())


def dysj(request):
    username = request.session['username']
    return render(request, 'dysj.html', locals())



def get_data(request):
    keyword = request.GET.get('name')
    page = int(request.GET.get("page", ''))
    limit = request.GET.get("limit", '')
    response_data = {}
    response_data['code'] = 0
    response_data['msg'] = ''
    data = []
    with open(os.path.join(work_dir, 'movie/data/recentlyMovies.csv'), 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for arrTxt in csv_reader:
            dict_values = {
                'id':arrTxt[0],
                'name':arrTxt[1],
                'pf':arrTxt[2],
                'sj':arrTxt[3],
                'dqcc':arrTxt[4],
                'dqrc':arrTxt[5],
                'pfzb':arrTxt[7],
                'ljsyts':arrTxt[8],
                'tpp':arrTxt[10],
                'my':arrTxt[11],
                'db':arrTxt[12],

            }
            data.append(dict_values)
        if page == 1:
            results = data[1:11]
        elif page == 2:
            results = data[11:21]
        elif page == 3:
            results = data[21:31]
        elif page == 4:
            results = data[31:41]
        elif page == 5:
            results = data[41:51]
        elif page == 6:
            results = data[51:61]
        elif page == 7:
            results = data[61:71]
        response_data['count'] = len(data)
        response_data['data'] = results

    return JsonResponse(response_data)


def update_data(request):
    from movies.getData import recently
    recently()
    return JsonResponse({'msg':'ok'})


def pfyc(request):
    data =[]
    value = []
    with open(os.path.join(work_dir, 'movie/data/predict_result.csv'), 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for arrTxt in csv_reader:
            data.append(arrTxt[0])

            value.append(arrTxt[1])
    data = data[1:]
    value=value[1:]
    print(data)
    print(value)
    return render(request,'pfyc.html',locals())