import os
import webbrowser
from threading import Thread
from tkinter import *
from tkinter import ttk
import tkinter as tk
from tkinter.messagebox import showerror

from getData import recently, history, showing, predict_data, hotMovies, special, champion_year, Tablets
from pyec import Showing, History, Industry
import numpy as np
import pandas as pd


def thread_it(func, *args):
    '''
    将函数打包进线程
    '''
    # 创建
    t = Thread(target=func, args=args)
    # 守护
    t.setDaemon(True)
    # 启动
    t.start()


class uiob:
    treeview = None
    vbar = None
    hbar = None
    path = str(os.getcwd().split("\\")[-1])

    def create_tree_showing(self):
        # 表格
        columns = ("排名", "影片名称", '当前票房', '上映日期', '累计票房', '当前场次', '当前人次', '票房占比', '累计上映天数',
                   '当前统计天数', '黄金场票房', '黄金场场次', '黄金场票房占比', '黄金场场次占比', '黄金场人次占比')
        treeview = ttk.Treeview(self.frame_l, height=30, show="headings", columns=columns)
        treeview.column("排名", width=50, anchor='center')
        treeview.column("影片名称", width=100, anchor='center')
        treeview.column("当前票房", width=75, anchor='center')
        treeview.column("上映日期", width=100, anchor='center')
        treeview.column("累计票房", width=75, anchor='center')
        treeview.column("当前场次", width=75, anchor='center')
        treeview.column("当前人次", width=75, anchor='center')
        treeview.column("票房占比", width=50, anchor='center')
        treeview.column("累计上映天数", width=50, anchor='center')
        treeview.column("当前统计天数", width=50, anchor='center')
        treeview.column("黄金场票房", width=75, anchor='center')
        treeview.column("黄金场场次", width=50, anchor='center')
        treeview.column("黄金场票房占比", width=50, anchor='center')
        treeview.column("黄金场场次占比", width=50, anchor='center')
        treeview.column("黄金场人次占比", width=50, anchor='center')

        treeview.heading("排名", text="排名")  # 显示表头
        treeview.heading("影片名称", text="影片名称")
        treeview.heading("当前票房", text="当前票房")
        treeview.heading("上映日期", text="上映日期")
        treeview.heading("累计票房", text="累计票房")
        treeview.heading("当前场次", text="当前场次")
        treeview.heading("当前人次", text="当前人次")
        treeview.heading("票房占比", text="票房占比")
        treeview.heading("累计上映天数", text="累计上映天数")
        treeview.heading("当前统计天数", text="当前统计天数")
        treeview.heading("黄金场票房", text="黄金场票房")
        treeview.heading("黄金场场次", text="黄金场场次")
        treeview.heading("黄金场票房占比", text="黄金场票房占比")
        treeview.heading("黄金场场次占比", text="黄金场场次占比")
        treeview.heading("黄金场人次占比", text="黄金场人次占比")

        # 垂直滚动条
        vbar = ttk.Scrollbar(self.frame_r, command=treeview.yview)
        treeview.configure(yscrollcommand=vbar.set)

        treeview.pack()
        self.treeview = treeview
        vbar.pack(side=RIGHT, fill=Y)
        self.vbar = vbar

    def create_tree_tablets(self):
        # 表格
        columns = ("排名", "影片名称", '上映天数', '影片类型', '总场次', '总场次环比', '上午场次', '上午场次环比', '下午场次',
                   '下午场次环比', '黄金场次', '黄金场次环比', '加映场次', '加映场次环比')
        treeview = ttk.Treeview(self.frame_l, height=30, show="headings", columns=columns)
        treeview.column("排名", width=50, anchor='center')
        treeview.column("影片名称", width=100, anchor='center')
        treeview.column("上映天数", width=75, anchor='center')
        treeview.column("影片类型", width=75, anchor='center')
        treeview.column("总场次", width=75, anchor='center')
        treeview.column("总场次环比", width=75, anchor='center')

        treeview.heading("排名", text="排名")  # 显示表头
        treeview.heading("影片名称", text="影片名称")
        treeview.heading("上映天数", text="上映天数")
        treeview.heading("影片类型", text="影片类型")
        treeview.heading("总场次", text="总场次")
        treeview.heading("总场次环比", text="总场次环比")


        # 垂直滚动条
        vbar = ttk.Scrollbar(self.frame_r, command=treeview.yview)
        treeview.configure(yscrollcommand=vbar.set)

        treeview.pack()
        self.treeview = treeview
        vbar.pack(side=RIGHT, fill=Y)
        self.vbar = vbar

    def create_tree_history(self):
        # 表格
        columns = ("排名", "影片名称", "影片国家", "影片主分类", "影片时长",
                   "上映日期", "总票房(万)", "总场次", "总人数(万)", "首日票房(万)",
                   "首周票房(万)", "首周末票房(万)", "点映票房(万)", "平均票价",
                   "场均人次", "上映天数", "猫眼想看人数", "猫眼评分", "淘票票想看人数", "淘票票评分",
                   "豆瓣想看人数", "豆瓣评分", "网页相关新闻数", "微信公众号新闻数")
        treeview = ttk.Treeview(self.frame_l, height=30, show="headings", columns=columns)
        treeview.column("排名", width=50, anchor='center')
        treeview.column("影片名称", width=100, anchor='center')
        treeview.column("影片国家", width=50, anchor='center')
        treeview.column("影片主分类", width=50, anchor='center')
        treeview.column("影片时长", width=50, anchor='center')
        treeview.column("上映日期", width=75, anchor='center')
        treeview.column("总票房(万)", width=75, anchor='center')
        treeview.column("总场次", width=75, anchor='center')
        treeview.column("总人数(万)", width=75, anchor='center')
        treeview.column("首日票房(万)", width=75, anchor='center')
        treeview.column("首周票房(万)", width=75, anchor='center')
        treeview.column("首周末票房(万)", width=75, anchor='center')
        treeview.column("点映票房(万)", width=50, anchor='center')
        treeview.column("平均票价", width=40, anchor='center')
        treeview.column("场均人次", width=40, anchor='center')
        treeview.column("上映天数", width=50, anchor='center')
        treeview.column("猫眼想看人数", width=75, anchor='center')
        treeview.column("猫眼评分", width=40, anchor='center')
        treeview.column("淘票票想看人数", width=75, anchor='center')
        treeview.column("淘票票评分", width=40, anchor='center')
        treeview.column("豆瓣想看人数", width=75, anchor='center')
        treeview.column("豆瓣评分", width=40, anchor='center')
        treeview.column("网页相关新闻数", width=50, anchor='center')
        treeview.column("微信公众号新闻数", width=50, anchor='center')

        treeview.heading("排名", text="排名")
        treeview.heading("影片名称", text="影片名称")
        treeview.heading("影片名称", text="影片名称")
        treeview.heading("影片主分类", text="影片主分类")
        treeview.heading("影片时长", text="影片时长")
        treeview.heading("上映日期", text="上映日期")
        treeview.heading("总票房(万)", text="总票房(万)")
        treeview.heading("总场次", text="总场次")
        treeview.heading("总人数(万)", text="总人数(万)")
        treeview.heading("首日票房(万)", text="首日票房(万)")
        treeview.heading("首周票房(万)", text="首周票房(万)")
        treeview.heading("首周末票房(万)", text="首周末票房(万)")
        treeview.heading("点映票房(万)", text="点映票房(万)")
        treeview.heading("平均票价", text="平均票价")
        treeview.heading("场均人次", text="场均人次")
        treeview.heading("上映天数", text="上映天数")
        treeview.heading("猫眼想看人数", text="猫眼想看人数")
        treeview.heading("猫眼评分", text="猫眼评分")
        treeview.heading("淘票票想看人数", text="淘票票想看人数")
        treeview.heading("淘票票评分", text="淘票票评分")
        treeview.heading("豆瓣想看人数", text="豆瓣想看人数")
        treeview.heading("豆瓣评分", text="豆瓣评分")
        treeview.heading("网页相关新闻数", text="网页相关新闻数")
        treeview.heading("微信公众号新闻数", text="微信公众号新闻数")

        # 垂直滚动条
        vbar = ttk.Scrollbar(self.frame_r, command=treeview.yview)
        treeview.configure(yscrollcommand=vbar.set)

        treeview.pack()
        self.treeview = treeview
        vbar.pack(side=RIGHT, fill=Y)
        self.vbar = vbar

    def create_tree_predict(self):
        columns = ("影片名称", "预测票房")
        treeview = ttk.Treeview(self.frame_l, height=30, show="headings", columns=columns)
        treeview.column("影片名称", width=100, anchor='center')
        treeview.column("预测票房", width=100, anchor='center')
        treeview.heading("影片名称", text="影片名称")
        treeview.heading("预测票房", text="预测票房(亿)")
        # 垂直滚动条
        vbar = ttk.Scrollbar(self.frame_r, command=treeview.yview)
        treeview.configure(yscrollcommand=vbar.set)
        treeview.pack()
        self.treeview = treeview
        vbar.pack(side=RIGHT, fill=Y)
        self.vbar = vbar

    def clear_tree(self, tree):
        '''
        清空表格
        '''
        tree.destroy()
        self.vbar.destroy()

    def add_tree(self, list, tree):
        '''
        新增数据到表格
        '''
        i = 0
        for subList in list:
            tree.insert('', 'end', values=subList)
            i = i + 1
        tree.grid()

    def showing(self):
        # showerror(title="失败", message="系统爬虫失效或超时，请联系系统开发者")
        if self.treeview is not None:
            self.clear_tree(self.treeview)  # 清空表格
        self.create_tree_showing()
        self.B_0['text'] = '正在努力搜索'
        showing()
        list = np.array(pd.read_csv("data/recentlyMovies.csv", encoding='gbk')).tolist()
        self.add_tree(list, self.treeview)  # 将数据添加到tree中

        self.B_0['state'] = NORMAL
        self.B_0['text'] = '在映电影'

        # normal()

    def history(self):
        # showerror(title="失败", message="历史电影数据获取失败")
        if self.treeview is not None:
            self.clear_tree(self.treeview)  # 清空表格
        self.create_tree_history()
        self.B_2['text'] = '正在努力搜索'
        list = history()
        self.add_tree(list, self.treeview)

        self.B_2['state'] = NORMAL
        self.B_2['text'] = '历史电影'

    def predict(self):
        # showerror(title="失败", message="在映电影票房预测失败")
        if self.treeview is not None:
            self.clear_tree(self.treeview)  # 清空表格
        self.create_tree_predict()
        self.B_4['text'] = '正在努力搜索'
        list = predict_data()
        self.add_tree(list, self.treeview)

        self.B_4['state'] = NORMAL
        self.B_4['text'] = '在映电影票房预测'

    def tablets(self):
        # showerror(title="失败", message="获取排片数据失败")
        if self.treeview is not None:
            self.clear_tree(self.treeview)  # 清空表格
        self.create_tree_tablets()
        self.B_6['text'] = '正在努力搜索'

        list = Tablets()
        self.add_tree(list, self.treeview)

        self.B_6['state'] = NORMAL
        self.B_6['text'] = '排片分析'

    def center_window(self, root, w, h):
        """
        窗口居于屏幕中央
        :param root: root
        :param w: 窗口宽度
        :param h: 窗口高度
        :return:
        """
        # 获取屏幕 宽、高
        ws = root.winfo_screenwidth()
        hs = root.winfo_screenheight()

        # 计算 x, y 位置
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)

        root.geometry('%dx%d+%d+%d' % (w, h, x, y))

    def clicking(self):
        # showerror(title="失败", message="跳转在映电影数据分析失败")
        recently()
        Showing()
        webbrowser.open("../templates/zydyfx.html")

    def clicked(self):
        # showerror(title="失败", message="跳转电影票房分析失败")
        History()
        webbrowser.open("../templates/pffx.html")

    def industry(self):
        # showerror(title="失败", message="跳转数据大盘失败")
        hotMovies()
        special()
        champion_year()
        Industry()
        webbrowser.open("../templates/sjdp.html")

    def ui_process(self):
        root = Tk()
        self.root = root

        root.title("电影数据分析")
        self.center_window(root, 1500, 750)
        root.resizable(0, 0)
        root['highlightcolor'] = 'yellow'

        labelframe = LabelFrame(root, width=1500, height=750, background="white")
        labelframe.place(x=5, y=5)
        self.labelframe = labelframe
        # 图片


        # 查询按钮
        B_0 = Button(labelframe, text="在映电影", background="white")
        B_0.place(x=400, y=25, width=100, height=50)
        self.B_0 = B_0
        B_0.configure(command=lambda: thread_it(self.showing()))  # 按钮绑定单击事件

        B_1 = Button(labelframe, text="在映前十数据分析", background="white")
        B_1.place(x=550, y=25, width=100, height=50)
        self.B_1 = B_1
        B_1.configure(command=lambda: thread_it(self.clicking()))  # 按钮绑定单击事件

        B_4 = Button(labelframe, text="在映电影票房预测", background="white")
        B_4.place(x=700, y=25, width=100, height=50)
        self.B_4 = B_4
        B_4.configure(command=lambda: thread_it(self.predict()))

        B_2 = Button(labelframe, text="历史电影", background="white")
        B_2.place(x=850, y=25, width=100, height=50)
        self.B_2 = B_2
        B_2.configure(command=lambda: thread_it(self.history()))

        B_3 = Button(labelframe, text="历史数据分析", background="white")
        B_3.place(x=1000, y=25, width=100, height=50)
        self.B_3 = B_3
        B_3.configure(command=lambda: thread_it(self.clicked()))

        B_5 = Button(labelframe, text="数据大盘", background="white")
        B_5.place(x=1150, y=25, width=100, height=50)
        self.B_5 = B_5
        B_5.configure(command=lambda: thread_it(self.industry()))

        B_6 = Button(labelframe, text="排片分析", background="white")
        B_6.place(x=1300, y=25, width=100, height=50)
        self.B_6 = B_6
        B_6.configure(command=lambda: thread_it(self.tablets()))

        # 框架布局，承载多个控件
        frame_root = Frame(labelframe)
        frame_l = Frame(frame_root)
        frame_r = Frame(frame_root)
        self.frame_root = frame_root
        self.frame_l = frame_l
        self.frame_r = frame_r

        # 框架的位置布局
        frame_l.grid(row=0, column=0, sticky=NSEW)
        frame_r.grid(row=0, column=1, sticky=NS)
        frame_root.place(x=10, y=100)

        root.columnconfigure(0, weight=1)
        root.mainloop()
