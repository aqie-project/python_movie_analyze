from collections import Counter

from pyecharts.charts import Bar, WordCloud, Pie, Page, Grid, Scatter, Line
import re
from pyecharts import options as opts
import pandas as pd
from pyecharts.globals import ThemeType


def remove_markers(str_list):
    pattern = re.compile(r'[^\u4e00-\u9fa5]')
    return [pattern.sub('', line) for line in str_list]


def History():
    csv_file = 'data/moviesBoxOffice.csv'  # 导入csv数据'
    data = pd.read_csv(csv_file, encoding='gbk')

    data_type = data['影片主分类'].value_counts()
    data_BoxOffice = data['总票房(万)'][:10]
    a = (
        Bar(init_opts=opts.InitOpts(height="450px", width="900px", theme=ThemeType.MACARONS, bg_color='white'))
            .add_xaxis(list(data_type.index))
            .add_yaxis("类型", list(data_type))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="票房TOP1000类型统计"),
            # datazoom_opts=[opts.DataZoomOpts(), opts.DataZoomOpts(type_="inside")],
        )
    )
    b = (
        Bar(init_opts=opts.InitOpts(height="450px", width="900px", theme=ThemeType.LIGHT))
            .add_xaxis(list(data['影片名称'][:10]))
            .add_yaxis("票房", list(data_BoxOffice))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="票房TOP10总票房统计"),
            # datazoom_opts=[opts.DataZoomOpts(), opts.DataZoomOpts(type_="inside")],
            xaxis_opts=opts.AxisOpts(axislabel_opts={"rotate": 20}))
    )
    c = (
        Line(init_opts=opts.InitOpts(height="450px", width="900px", theme=ThemeType.LIGHT))
            .add_xaxis(list(data['影片名称'][:10]))
            .add_yaxis("总票房(万)", list(data['总票房(万)'][:10]), is_smooth=True)
            .add_yaxis("首日票房(万)", list(data['首日票房(万)'][:10]), is_smooth=True)
            .add_yaxis("首周票房(万)", list(data['首周票房(万)'][:10]), is_smooth=True)
            .add_yaxis("首周末票房(万)", list(data['首周末票房(万)'][:10]), is_smooth=True)
            .add_yaxis("点映票房(万)", list(data['点映票房(万)'][:10]), is_smooth=True)
            .set_series_opts(label_opts=opts.LabelOpts(is_show=True),
                             linestyle_opts=opts.LineStyleOpts(width=3))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="票房TOP10各类票房统计", pos_left='top'),
            # datazoom_opts=[opts.DataZoomOpts(), opts.DataZoomOpts(type_="inside")],
            xaxis_opts=opts.AxisOpts(name="影片名称", axislabel_opts={"rotate": 20}),
            yaxis_opts=opts.AxisOpts(name="票房(万)")
        )
    )
    d = (
        Line(init_opts=opts.InitOpts(height="450px", width="900px", theme=ThemeType.MACARONS, bg_color='white'))
            .add_xaxis(list(data['影片名称'][:10]))
            .add_yaxis("总场次", list(data['总场次'][:10]), is_smooth=True)
            .add_yaxis("总人次(万)", list(data['总人次(万)'][:10]), is_smooth=True)
            .set_series_opts(label_opts=opts.LabelOpts(is_show=True),
                             linestyle_opts=opts.LineStyleOpts(width=3))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="票房TOP10场次人次统计", pos_left='top'),
            # datazoom_opts=[opts.DataZoomOpts(), opts.DataZoomOpts(type_="inside")],
            xaxis_opts=opts.AxisOpts(name="影片名称", axislabel_opts={"rotate": 20}),
            yaxis_opts=opts.AxisOpts(name="次(万)")
        )
    )
    e = (
        Line(init_opts=opts.InitOpts(height="450px", width="900px", theme=ThemeType.LIGHT))
            .add_xaxis(list(data['影片名称'][:10]))
            .add_yaxis("平均票价", list(data['平均票价'][:10]), is_smooth=True)
            .add_yaxis("场均人次", list(data['场均人次'][:10]), is_smooth=True)
            .add_yaxis("上映天数", list(data['上映天数'][:10]), is_smooth=True)
            .set_series_opts(label_opts=opts.LabelOpts(is_show=True),
                             linestyle_opts=opts.LineStyleOpts(width=3))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="票房TOP10票价统计", pos_left='top'),
            xaxis_opts=opts.AxisOpts(name="影片名称", axislabel_opts={"rotate": 20})
        )
    )
    f = (
        Line(init_opts=opts.InitOpts(height="450px", width="900px", theme=ThemeType.MACARONS, bg_color='white'))
            .add_xaxis(list(data['影片名称'][:10]))
            .add_yaxis("猫眼想看人数", list(data['猫眼想看人数'][:10]), is_smooth=True)
            .add_yaxis("淘票票想看人数", list(data['淘票票想看人数'][:10]), is_smooth=True)
            .add_yaxis("豆瓣想看人数", list(data['豆瓣想看人数'][:10]), is_smooth=True)
            .add_yaxis("网页相关新闻数", list(data['网页相关新闻数'][:10]), is_smooth=True)
            .add_yaxis("微信公众号新闻数", list(data['微信公众号新闻数'][:10]), is_smooth=True)
            .set_series_opts(label_opts=opts.LabelOpts(is_show=True),
                             linestyle_opts=opts.LineStyleOpts(width=3))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="票房TOP10舆情统计", pos_left='top'),
            xaxis_opts=opts.AxisOpts(name="影片名称", axislabel_opts={"rotate": 20}),
            yaxis_opts=opts.AxisOpts(name="数量")
        )
    )
    g = (
        Line(init_opts=opts.InitOpts(height="450px", width="900px"))
            .add_xaxis(list(data['影片名称'][:10]))
            .add_yaxis("猫眼评分", list(data['猫眼评分'][:10]), is_smooth=True)
            .add_yaxis("淘票票评分", list(data['淘票票评分'][:10]), is_smooth=True)
            .add_yaxis("豆瓣评分", list(data['豆瓣评分'][:10]), is_smooth=True)
            .set_series_opts(label_opts=opts.LabelOpts(is_show=True),
                             linestyle_opts=opts.LineStyleOpts(width=3))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="票房TOP10口碑统计", pos_left='top'),
            xaxis_opts=opts.AxisOpts(name="影片名称", axislabel_opts={"rotate": 20})
        )
    )
    words_list = []
    for w in data['影片名称']:
        words_list.append(w)
    c1 = Counter(words_list)
    h = (
        WordCloud(init_opts=opts.InitOpts(height="450px", width="900px"))
            .add(series_name="影片词云", data_pair=c1.most_common(), word_size_range=[22, 66])
            .set_global_opts(
            title_opts=opts.TitleOpts(
                title="片名词云", title_textstyle_opts=opts.TextStyleOpts(font_size=23)
            ),
            tooltip_opts=opts.TooltipOpts(is_show=True),
        )
    )
    i = (
        Pie(init_opts=opts.InitOpts(height="450px", width="600px"))
            .add(
            "",
            [list(z) for z in zip(data_type.index, list(data_type))],
            radius=["40%", "75%"],
        )
            .set_global_opts(
            title_opts=opts.TitleOpts(title="电影分类"),
            legend_opts=opts.LegendOpts(orient="vertical", pos_top="15%", pos_left="2%"),
        )
            .set_series_opts(label_opts=opts.LabelOpts(formatter="{b}: {c}"))
    )
    page = (
        Page(page_title="电影票房分析", layout=Page.SimplePageLayout)
            .add(a)
            .add(b)
            .add(h)
            .add(i)
            .add(c)
            .add(d)
            .add(e)
            .add(f)
            .add(g)
            .render("pffx.html")
    )


def Showing():
    data = pd.read_csv("data/top10_data.csv", encoding='gbk')
    a = (
        Bar(init_opts=opts.InitOpts(height="450px", width="600px", theme=ThemeType.MACARONS))
            .add_xaxis(list(data['影片名称']))
            .add_yaxis('票房', list(data['影片票房'] / 10000))
            .reversal_axis()
            .set_series_opts(label_opts=opts.LabelOpts(position="right"))
            .set_global_opts(title_opts=opts.TitleOpts(title="影片详情-票房", pos_left="10%"),
                             yaxis_opts=opts.AxisOpts(axislabel_opts={"rotate": 300}, name="电影名"),
                             xaxis_opts=opts.AxisOpts(name="票房(万)"))
    )
    b = (
        Bar(init_opts=opts.InitOpts(height="450px", width="600px"))
            .add_xaxis(list(data['影片名称']))
            .add_yaxis('场次', list(data['影片场次']))
            .reversal_axis()
            .set_series_opts(label_opts=opts.LabelOpts(position="right"))
            .set_global_opts(title_opts=opts.TitleOpts(title="影片详情-场次", pos_left="10%"),
                             yaxis_opts=opts.AxisOpts(axislabel_opts={"rotate": 300}, name="电影名"),
                             xaxis_opts=opts.AxisOpts(name="场次"))
    )
    c = (
        Bar(init_opts=opts.InitOpts(height="450px", width="600px", theme=ThemeType.ESSOS))
            .add_xaxis(list(data['影片名称']))
            .add_yaxis('人次', list(data['影片人次']))
            .reversal_axis()
            .set_series_opts(label_opts=opts.LabelOpts(position="right"))
            .set_global_opts(title_opts=opts.TitleOpts(title="影片详情-人次", pos_left="10%"),
                             yaxis_opts=opts.AxisOpts(axislabel_opts={"rotate": 300}, name="电影名"),
                             xaxis_opts=opts.AxisOpts(name="人次"))
    )
    d = (
        Bar(init_opts=opts.InitOpts(height="450px", width="600px", theme=ThemeType.LIGHT))
            .add_xaxis(list(data['影片名称']))
            .add_yaxis('上座率', list(data['上座率']))
            .reversal_axis()
            .set_series_opts(label_opts=opts.LabelOpts(position="right"))
            .set_global_opts(title_opts=opts.TitleOpts(title="影片详情-上座率", pos_left="10%"),
                             yaxis_opts=opts.AxisOpts(axislabel_opts={"rotate": 300}, name="电影名"),
                             xaxis_opts=opts.AxisOpts(name="上座率"))
    )
    e = (
        Pie(init_opts=opts.InitOpts(height="450px", width="600px"))
            .add(
            "",
            [list(z) for z in zip(data['影片名称'], list(data['影片票房占比']))],
            radius=["40%", "75%"],
            center=["60%", "50%"],
        )
            .set_global_opts(
            title_opts=opts.TitleOpts(title="影片票房占比"),
            legend_opts=opts.LegendOpts(orient="vertical", pos_top="10%", pos_left="0%"),
        )
            .set_series_opts(label_opts=opts.LabelOpts(formatter="{b}: {c}%"))
    )
    f = (
        Pie(init_opts=opts.InitOpts(height="450px", width="600px"))
            .add(
            "",
            [list(z) for z in zip(data['影片名称'], list(data['影片场次占比']))],
            radius=["40%", "75%"],
            center=["60%", "50%"],
        )
            .set_global_opts(
            title_opts=opts.TitleOpts(title="影片场次占比"),
            legend_opts=opts.LegendOpts(orient="vertical", pos_top="10%", pos_left="0%"))
            .set_series_opts(label_opts=opts.LabelOpts(formatter="{b}: {c}%"))
    )
    g = (
        Pie(init_opts=opts.InitOpts(height="450px", width="600px"))
            .add(
            "",
            [list(z) for z in zip(data['影片名称'], list(data['影片人次占比']))],
            radius=["40%", "75%"],
            center=["60%", "50%"],
        )
            .set_global_opts(
            title_opts=opts.TitleOpts(title="影片人次占比"),
            legend_opts=opts.LegendOpts(orient="vertical", pos_top="10%", pos_left="0%"))
            .set_series_opts(label_opts=opts.LabelOpts(formatter="{b}: {c}%"))
    )
    h = (
        Bar(init_opts=opts.InitOpts(height="450px", width="600px", theme=ThemeType.LIGHT))
            .add_xaxis(list(data['影片名称']))
            .add_yaxis('一线城市票房', list(data['一线城市票房']), stack='stack1')
            .add_yaxis('二线城市票房', list(data['二线城市票房']), stack='stack1')
            .add_yaxis('三线城市票房', list(data['三线城市票房']), stack='stack1')
            .add_yaxis('四线城市票房', list(data['四线城市票房']), stack='stack1')
            .add_yaxis('其它票房', list(data['其它票房']), stack='stack1')
            .reversal_axis()
            .set_series_opts(label_opts=opts.LabelOpts(position="right", is_show=False))
            .set_global_opts(title_opts=opts.TitleOpts(title="影片地域分布-票房", pos_left="10%"),
                             yaxis_opts=opts.AxisOpts(axislabel_opts={"rotate": 300}, name="电影名"),
                             xaxis_opts=opts.AxisOpts(name="票房"),
                             legend_opts=opts.LegendOpts(pos_left="40%"))
    )
    i = (
        Bar(init_opts=opts.InitOpts(height="450px", width="600px", theme=ThemeType.LIGHT))
            .add_xaxis(list(data['影片名称']))
            .add_yaxis('一线城市场次', list(data['一线城市场次']), stack='stack1')
            .add_yaxis('二线城市场次', list(data['二线城市场次']), stack='stack1')
            .add_yaxis('三线城市场次', list(data['三线城市场次']), stack='stack1')
            .add_yaxis('四线城市场次', list(data['四线城市场次']), stack='stack1')
            .add_yaxis('其它场次', list(data['其它场次']), stack='stack1')
            .reversal_axis()
            .set_series_opts(label_opts=opts.LabelOpts(position="right", is_show=False))
            .set_global_opts(title_opts=opts.TitleOpts(title="影片地域分布-场次", pos_left="10%"),
                             yaxis_opts=opts.AxisOpts(axislabel_opts={"rotate": 300}, name="电影名"),
                             xaxis_opts=opts.AxisOpts(name="场次"),
                             legend_opts=opts.LegendOpts(pos_left="40%"))
    )
    j = (
        Bar(init_opts=opts.InitOpts(height="450px", width="600px", theme=ThemeType.LIGHT))
            .add_xaxis(list(data['影片名称']))
            .add_yaxis('一线城市人次', list(data['一线城市人次']), stack='stack1')
            .add_yaxis('二线城市人次', list(data['二线城市人次']), stack='stack1')
            .add_yaxis('三线城市人次', list(data['三线城市人次']), stack='stack1')
            .add_yaxis('四线城市人次', list(data['四线城市人次']), stack='stack1')
            .add_yaxis('其它人次', list(data['其它人次']), stack='stack1')
            .reversal_axis()
            .set_series_opts(label_opts=opts.LabelOpts(position="right", is_show=False))
            .set_global_opts(title_opts=opts.TitleOpts(title="影片地域分布-人次", pos_left="10%"),
                             yaxis_opts=opts.AxisOpts(axislabel_opts={"rotate": 300}, name="电影名"),
                             xaxis_opts=opts.AxisOpts(name="人次"),
                             legend_opts=opts.LegendOpts(pos_left="40%"))
    )
    page = (
        Page(page_title="在映电影分析", layout=Page.SimplePageLayout)
            .add(a)
            .add(b)
            .add(c)
            .add(d)
            .add(e)
            .add(f)
            .add(g)
            .add(h)
            .add(i)
            .add(j)
            .render("zydyfx.html")
    )


def Industry():
    top_movies = pd.read_csv("data/top_movie.csv", encoding='gbk')
    special_movies = pd.read_csv("data/special.csv", encoding='gbk')
    champion_movies = pd.read_csv("data/movies_champion.csv", encoding='gbk')
    year_movies = pd.read_csv("data/movies_year.csv", encoding='gbk')
    a = (
        Line(init_opts=opts.InitOpts(height="450px", width="900px", theme=ThemeType.LIGHT))
            .add_xaxis(list(top_movies['日期']))
            .add_yaxis(top_movies.columns[1], list(top_movies[top_movies.columns[1]]), is_smooth=True)
            .add_yaxis(top_movies.columns[2], list(top_movies[top_movies.columns[2]]), is_smooth=True)
            .add_yaxis(top_movies.columns[3], list(top_movies[top_movies.columns[3]]), is_smooth=True)
            .add_yaxis(top_movies.columns[4], list(top_movies[top_movies.columns[4]]), is_smooth=True)
            .add_yaxis(top_movies.columns[5], list(top_movies[top_movies.columns[5]]), is_smooth=True)
            .set_series_opts(label_opts=opts.LabelOpts(is_show=False),
                             linestyle_opts=opts.LineStyleOpts(width=3))
            .set_global_opts(
            title_opts=opts.TitleOpts(title="热门影片票房趋势", pos_left='top'),
            xaxis_opts=opts.AxisOpts(axislabel_opts={"rotate": 20}),
            yaxis_opts=opts.AxisOpts(name="票房(万)", position="left"),
        )
    )
    b = (
        Pie(init_opts=opts.InitOpts(height="450px", width="600px"))
            .add(
            "",
            [list(z) for z in zip(special_movies['特效厅种类'], list(special_movies['特效厅票房占比']))],
            radius=["40%", "75%"],
            center=["60%", "50%"],
        )
            .set_global_opts(
            title_opts=opts.TitleOpts(title="特效厅票房占比分布"),
            legend_opts=opts.LegendOpts(orient="vertical", pos_top="10%", pos_left="0%"))
            .set_series_opts(label_opts=opts.LabelOpts(formatter="{b}: {c}%"))
    )

    bar1 = (
        Bar(init_opts=opts.InitOpts(height="450px", width="900px", theme=ThemeType.LIGHT))
        .add_xaxis(list(champion_movies['影片年份']))
        .add_yaxis('票房', list(champion_movies['影片票房']), z=0)
        .set_series_opts(label_opts=opts.LabelOpts(position="right", is_show=False))
        .extend_axis(
            yaxis=opts.AxisOpts(name="票房占比", axislabel_opts=opts.LabelOpts(formatter="{value}"))
        )
        .set_global_opts(title_opts=opts.TitleOpts(title="历年冠军影片票房趋势", pos_left="10%"),
                     yaxis_opts=opts.AxisOpts(name="票房(亿)", position="left"),
                     legend_opts=opts.LegendOpts(pos_left="40%"))
    )

    year_list1 = []
    for i in champion_movies['影片年份']:
        year_list1.append(str(i))

    line1 = (
        Line(init_opts=opts.InitOpts(height="450px", width="900px"))
        .add_xaxis(year_list1)
        .add_yaxis("票房占比", list(champion_movies['影片票房占比']), is_smooth=True, yaxis_index=1)
        .set_series_opts(label_opts=opts.LabelOpts(is_show=False),
                         linestyle_opts=opts.LineStyleOpts(width=3))
        .set_global_opts(
            title_opts=opts.TitleOpts(title="热门影片票房趋势", pos_left='top'),
            yaxis_opts=opts.AxisOpts(name="票房占比", position='right'),
        )
    )
    bar1.overlap(line1)

    year_list2 = []
    for i in year_movies['年份']:
        year_list2.append(str(i))

    line2 = (
        Line(init_opts=opts.InitOpts(height="450px", width="900px", theme=ThemeType.LIGHT))
        .add_xaxis(year_list2)
        .add_yaxis("新上映影片总数", list(year_movies['新上映影片总数']), is_smooth=True, yaxis_index=1)
        .add_yaxis("新上映国产数", list(year_movies['新上映国产数']), is_smooth=True)
        .add_yaxis("新上映进口数", list(year_movies['新上映进口数']), is_smooth=True)
        .set_series_opts(label_opts=opts.LabelOpts(is_show=False),
                         linestyle_opts=opts.LineStyleOpts(width=3))
        .set_global_opts(
            title_opts=opts.TitleOpts(title="热门影片票房趋势", pos_left='top'),
            yaxis_opts=opts.AxisOpts(name="影片数量", position='right'),
        ))

    bar2 = (
        Bar(init_opts=opts.InitOpts(height="450px", width="900px", theme=ThemeType.LIGHT))
        .add_xaxis(list(year_list2))
        .add_yaxis('总票房', list(year_movies['总票房']), z=0, stack="stack1")
        .add_yaxis('国产票房', list(year_movies['国产票房']), z=0, stack="stack1")
        .add_yaxis('进口票房', list(year_movies['进口票房']), z=0, stack="stack1")
        .set_series_opts(label_opts=opts.LabelOpts(position="right", is_show=False))
        .extend_axis(
            yaxis=opts.AxisOpts(name="影片数量", axislabel_opts=opts.LabelOpts(formatter="{value}"))
        )
        .set_global_opts(title_opts=opts.TitleOpts(title="影片年票房及新映影片趋势", pos_left="10%"),
                     yaxis_opts=opts.AxisOpts(name="票房(亿)", position="left"),
                     legend_opts=opts.LegendOpts(pos_left="40%")))

    bar2.overlap(line2)

    page = (
        Page(page_title="数据大盘", layout=Page.SimplePageLayout)
            .add(a)
            .add(b)
            .add(bar1)
            .add(bar2)
            .render("sjdp.html")
    )


if __name__ == '__main__':
    History()
